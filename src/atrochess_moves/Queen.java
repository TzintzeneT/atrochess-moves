package atrochess_moves;

public final class Queen extends Piece {
    public Queen(int color){
        super("Queen", color, 9);
    }

    protected Queen clonePiece() {
        Queen temp = new Queen(this.color);
        temp.posY = this.posY;
        temp.posX = this.posX;
        return temp;
    }
}
