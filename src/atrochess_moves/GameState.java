package atrochess_moves;

public enum GameState {
    Active,
    Checkmate,
    Stalemate
}
