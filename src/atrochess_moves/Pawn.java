package atrochess_moves;

public final class Pawn extends Piece {
    public Pawn(int color) {
        super("Pawn", color, 1);
    }

    public void promote(String name) {
        this.name = name;
        this.value = switch (name) {
            case "Bishop", "Knight" -> 3;
            case "Rook" -> 5;
            case "Queen" -> 9;
            default -> throw new IllegalStateException("Error: cannot promote to a: " + name);
        };
    }

    protected Pawn clonePiece() {
        Pawn temp = new Pawn(this.color);
        temp.posY = this.posY;
        temp.posX = this.posX;
        return temp;
    }
}
