package atrochess_moves;

import java.util.ArrayList;

public final class BishopMoves {

    protected static boolean moveBishop(Board board, Piece piece, int toY, int toX) {

        // if moving only in one axis (not possible for bishop)
        if (toY - piece.posY == 0 || toX - piece.posX == 0) {
            return false;
        }

        // calc the Y direction (up or down)
        int directionY = Math.abs(toY - piece.posY) / (toY - piece.posY);
        // calc the X direction (left or right)
        int directionX = Math.abs(toX - piece.posX) / (toX - piece.posX);

        int currentY = piece.posY + directionY;
        int currentX = piece.posX + directionX;

        // move diagonally square by square and check if there is any piece in the way to the destination. if there is, return false, otherwise return true.
        for (; currentY != toY && currentX != toX; currentY += directionY, currentX += directionX) {
            if (board.History.getLast().getPiece(currentY, currentX) != null) {
                return false;
            }
        }
        // if arrived at the desired destination - true, otherwise - false.
        return currentY == toY && currentX == toX;
    }

    protected static boolean canBishopMove(Board board, Piece piece) {

        for (int y = piece.posY, x = piece.posX; y < board.History.getLast().board[0].length && x < board.History.getLast().board.length; x++, y++) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                return true;
            }
        }

        for (int y = piece.posY, x = piece.posX; y >= 0 && x >= 0; x--, y--) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                return true;
            }
        }

        for (int y = piece.posY, x = piece.posX; y >= 0 && x < board.History.getLast().board.length; x++, y--) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                return true;
            }
        }

        for (int y = piece.posY, x = piece.posX; y < board.History.getLast().board[0].length && x >= 0; x--, y++) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                return true;
            }
        }
        return false;
    }

    protected static ArrayList<ChessMove> getAllBishopMoves(Board board, Piece piece) {

        ArrayList<ChessMove> chessMoves = new ArrayList<ChessMove>();

        for (int y = piece.posY, x = piece.posX; y < board.History.getLast().board[0].length && x < board.History.getLast().board.length; x++, y++) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                chessMoves.add(new ChessMove(piece.posY, piece.posX, y, x));
            }
        }

        for (int y = piece.posY, x = piece.posX; y >= 0 && x >= 0; x--, y--) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                chessMoves.add(new ChessMove(piece.posY, piece.posX, y, x));
            }
        }

        for (int y = piece.posY, x = piece.posX; y >= 0 && x < board.History.getLast().board.length; x++, y--) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                chessMoves.add(new ChessMove(piece.posY, piece.posX, y, x));
            }
        }

        for (int y = piece.posY, x = piece.posX; y < board.History.getLast().board[0].length && x >= 0; x--, y++) {
            if (PieceMoves.isMoveLegal(board, piece, y, x)) {
                chessMoves.add(new ChessMove(piece.posY, piece.posX, y, x));
            }
        }

        return chessMoves;
    }
}
