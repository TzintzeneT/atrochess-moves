package atrochess_moves;

abstract public class Piece {
    protected String name;
    protected final int color;
    protected int posX;
    protected int posY;
    protected int value;

    protected Piece(String name, int color, int value) {
        this.name = name;
        this.color = color;
        this.value = value;
    }

    protected void move(int y, int x) {
        this.posX = x;
        this.posY = y;
    }

    public int getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }

    public int getColor() {
        return this.color;
    }

    public int getPosY() {
        return this.posY;
    }

    public int getPosX() {
        return this.posX;
    }

    public String debugString() {
        return "type: " + this.name + ", Color: " + this.color + ", Pos: y=" + this.posY + " x=" + this.posX;
    }

    @Override
    public String toString() {
        return "type: " + this.name + ", Color: " + this.color;
    }

    abstract protected Piece clonePiece();
}
