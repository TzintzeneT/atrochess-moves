package atrochess_moves;

import java.util.ArrayList;

public final class QueenMoves {

    protected static boolean moveQueen(Board board, Piece piece, int toY, int toX) {

        return RookMoves.moveRook(board, piece, toY, toX) || BishopMoves.moveBishop(board, piece, toY, toX);
    }

    protected static boolean canQueenMove(Board board, Piece piece) {

        return RookMoves.canRookMove(board, piece) || BishopMoves.canBishopMove(board, piece);
    }

    protected static ArrayList<ChessMove> getALlQueenMoves(Board board, Piece piece) {

        ArrayList<ChessMove> chessMoves = new ArrayList<ChessMove>();
        chessMoves.addAll(RookMoves.getAllRookMoves(board, piece));
        chessMoves.addAll(BishopMoves.getAllBishopMoves(board, piece));
        return chessMoves;
    }
}
