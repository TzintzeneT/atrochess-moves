package atrochess_moves;

import java.util.ArrayList;

public final class RookMoves {

    protected static boolean moveRook(Board board, Piece piece, int toY, int toX) {

        // if the ROOOOKKK's X or Y movement is not 0 (the ROOOOKKK is moving in more than 1 direction)
        if (Math.min(Math.abs(toY - piece.posY), Math.abs(toX - piece.posX)) != 0) {
            return false;
        }

        if (Math.abs(toY - piece.posY) > 0) {

            // calc the Y direction (up -1 or down +1)
            int directionY = Math.abs(piece.posY - toY) / (-1 * (piece.posY - toY));

            for (int currentY = piece.posY + directionY; currentY != toY; currentY += directionY) {
                // if a piece is in the way
                if (board.History.getLast().getPiece(currentY, toX) != null) {
                    return false;
                }
            }
            return true;
        } else {

            // calc the X direction (left or right)
            int directionX = Math.abs(piece.posX - toX) / (-1 * (piece.posX - toX));

            for (int currentX = piece.posX + directionX; currentX != toX; currentX += directionX) {
                // if a piece is in the way
                if (board.History.getLast().getPiece(toY, currentX) != null) {
                    return false;
                }
            }
            return true;
        }
    }

    protected static boolean canRookMove(Board board, Piece piece) {

        for (int xMovement = 0; xMovement < board.History.getLast().board.length; xMovement++) {
            if (PieceMoves.isMoveLegal(board, piece, piece.posY, xMovement)) {
                return true;
            }
        }

        for (int yMovement = 0; yMovement < board.History.getLast().board[0].length; yMovement++) {
            if (PieceMoves.isMoveLegal(board, piece, yMovement, piece.posX)) {
                return true;
            }
        }
        return false;
    }

    protected static ArrayList<ChessMove> getAllRookMoves(Board board, Piece piece) {

        ArrayList<ChessMove> chessMoves = new ArrayList<ChessMove>();

        for (int xMovement = 0; xMovement < board.History.getLast().board.length; xMovement++) {
            if (PieceMoves.isMoveLegal(board, piece, piece.posY, xMovement)) {
                chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY, xMovement));
            }
        }

        for (int yMovement = 0; yMovement < board.History.getLast().board[0].length; yMovement++) {
            if (PieceMoves.isMoveLegal(board, piece, yMovement, piece.posX)) {
                chessMoves.add(new ChessMove(piece.posY, piece.posX, yMovement, piece.posX));
            }
        }
        return chessMoves;
    }
}
