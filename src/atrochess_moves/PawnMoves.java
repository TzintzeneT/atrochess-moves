package atrochess_moves;

import java.util.ArrayList;

public final class PawnMoves {

    protected static boolean movePawn(Board board, Piece piece, int toY, int toX) {

        // make sure that color 0 is going down (+1) and color 1 is going up (-1) [I know I am smort]
        final int direction = piece.color * -2 + 1;
        // calc the start position based on the color
        final int startPosY = piece.color * (board.defaultPosition.board.length - 1) + direction;
        // get the piece at the destination square
        final Piece pieceAtDestination = board.History.getLast().getPiece(toY, toX);

        // do you take a piece?
        if (pieceAtDestination == null) {
            // nope, there is nothing there

            // moved 3 squares?
            if (piece.posY == startPosY + direction * 3 &&

                    // is the piece that is being en passant is not null
                    board.History.getLast().getPiece(piece.posY, toX) != null &&
                    // is the piece that is being en passant is a pawn?
                    board.History.getLast().getPiece(piece.posY, toX).name.equals("Pawn") &&
                    // if moving only 1 square diagonally?
                    Math.abs(toX - piece.posX) == 1 && piece.posY + direction == toY &&
                    // if it is not the first move
                    !board.moves.isEmpty() &&
                    // is the pawn that you take is a different color?
                    board.History.getLast().getPiece(piece.posY, toX).color != piece.color &&
                    // is the last move was made by the pawn being en passant?
//                    board.History.getLast().getPiece(piece.posY, toX) == board.moves.getLast().piece &&
                    // was it a 2 square move?
                    Math.abs(board.moves.getLast().fromY - board.moves.getLast().toY) == 2) {

                // you are able to en passant
                return true;
            }

            // so don't move sideways
            if (toX != piece.posX) {
//                System.out.println("waaaaa i am sad");
                return false;
            }

            // if trying to move 2 squares
            if (toY == piece.posY + direction * 2) {
                // are you in a starting position + is the way empty?
                return piece.posY == startPosY && board.History.getLast().getPiece(piece.posY + direction, toX) == null;
            }

            // if you want to move 1 square you can
            return toY == piece.posY + direction;

            // there is a piece there
        } else {
            // you can only move one square diagonally + forward and take the piece if it is in different color
            return Math.abs(toX - piece.posX) == 1 && toY - piece.posY == direction && pieceAtDestination.color != piece.color;
        }
    }

    // check if a pawn have legal ChessBoard.moves
    protected static boolean canPawnMove(Board board, Piece piece) {

        // make sure that color 0 is going down (+1) and color 1 is going up (-1) [I know I am smort]
        final int direction = piece.color * -2 + 1;

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + direction, piece.posX)) {
            return true;
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY + direction * 2, piece.posX)) {
            return true;
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY + direction, piece.posX + 1)) {
            return true;
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY + direction, piece.posX - 1)) {
            return true;
        }
        return false;
    }

    private static boolean isMoveAPromotion(Board board, ChessMove chessMove) {
        return chessMove.toY == board.getBoardYLength()-1;
    }

    private static ArrayList<ChessMove> generatePromotionMoves(ChessMove chessMove) {
        String[] canPromoteTo = {"Queen", "Rook", "Bishop", "Knight"};
        ArrayList <ChessMove> promotions = new ArrayList<ChessMove>();

        // generate all available promotion moves
        for (String promotedPiece : canPromoteTo) {
            promotions.add(new ChessMove(chessMove.fromY, chessMove.fromX, chessMove.toY, chessMove.toX, promotedPiece));
        }

        // return all promotion available moves
        return promotions;
    }

    protected static ArrayList<ChessMove> getAllPawnMoves(Board board, Piece piece) {

        ArrayList<ChessMove> chessMoves = new ArrayList<ChessMove>();

        // make sure that color 0 is going down (+1) and color 1 is going up (-1) [I know I am smort]
        final int direction = piece.color * -2 + 1;

        int[][] moves = {{direction, 0}, {direction * 2, 0}, {direction, 1}, {direction, -1}};

        // check every possible move for pawn
        for (int[] move: moves) {
            if (PieceMoves.isMoveLegal(board, piece, piece.posY + move[0], piece.posX + move[1])) {

                // generate the move
                ChessMove chessMove = new ChessMove(piece.posY, piece.posX, piece.posY + move[0], piece.posX + move[1]);

                // is it a promotion?
                if (isMoveAPromotion(board, chessMove)) {

                    // if it is, generate & add promotion moves
                    chessMoves.addAll(generatePromotionMoves(chessMove));

                    // otherwise
                } else {

                    // add the move
                    chessMoves.add(chessMove);
                }
            }
        }
        return chessMoves;
    }

    protected static boolean promotePawn(Board board, Piece piece, int toY, int toX, String promotionName) {

//        // if it is not a pawn
//        if (!piece.getClass().equals(Pawn.class)){
//            return false;
//        }
//
//        Pawn pawn = (Pawn)(piece);
//        // make sure that color 0 is going down (+1) and color 1 is going up (-1) [I know I am smort]
//        final int direction = pawn.color * -2 + 1;
//        // calc the start position based on the color
//        final int startPosY = pawn.color * (board.defaultPosition.board.length - 1) + direction;
//
//        // if it is a not promoted pawn & it can move to the specified location & after moving it is at the end of the ChessBoard.board
//        if (pawn.name.equals("Pawn") && PieceMoves.isMoveLegal(board, pawn, toY, toX) && toY == startPosY + direction * 5) {
//            PieceMoves.movePiece(board, pawn, toY, toX);
//            pawn.promote(promotionName);
//            return true;
//        }
//        return false;

//        System.out.println("pass 71");

        if (canPawnPromote(board, piece, toY, toX) && promotionName != null) {

//            System.out.println("pass 72");
            Pawn pawn = (Pawn) (piece);
            PieceMoves.movePiece(board, pawn, toY, toX);
            pawn.promote(promotionName);
            System.out.println("promoted wooo");
            return true;
        }
//        System.out.println("failed 72 " + promotionName);
        return false;
    }

    protected static boolean isPawnAttacking(Board board, Piece piece, int toY, int toX) {

        final int direction = piece.color * -2 + 1;

        return piece.posY + direction == toY && Math.abs(toX - piece.posX) == 1;
    }

    public static boolean canPawnPromote(Board board, Piece piece, int toY, int toX) {

        // if it is not a pawn
        if (!piece.getClass().equals(Pawn.class)) {
            return false;
        }

//        System.out.println("pass 73");

        Pawn pawn = (Pawn) (piece);
        // make sure that color 0 is going down (+1) and color 1 is going up (-1) [I know I am smort]
        final int direction = pawn.color * -2 + 1;
        // calc the start position based on the color
        final int startPosY = pawn.color * (board.defaultPosition.board.length - 1) + direction;

        // if it is a not promoted pawn & it can move to the specified location & after moving it is at the end of the ChessBoard.board
        //            PieceMoves.movePiece(board, pawn, toY, toX);
        //            pawn.promote(promotionName);
        return (pawn.name.equals("Pawn") && PieceMoves.isMoveLegal(board, pawn, toY, toX) && toY == startPosY + direction * 6);
    }

    protected static boolean enPassant(Board board, Piece piece, int toY, int toX) {

        // is the pawn real? is the destination square empty
        if (piece == null || board.History.getLast().getPiece(toY, toX) != null) {
            return false;
        }

        // make sure that color 0 is going down (+1) and color 1 is going up (-1) [I know I am smort]
        final int direction = piece.color * -2 + 1;
        // calc the start position based on the color
        final int startPosY = piece.color * (board.defaultPosition.board.length - 1) + direction;


        if (piece.posY == startPosY + direction * 3 &&

                // is the piece that is being en passant is not null
                board.History.getLast().getPiece(piece.posY, toX) != null &&
                // is the piece that is being en passant is a pawn?
                board.History.getLast().getPiece(piece.posY, toX).name.equals("Pawn") &&
                // if moving only 1 square diagonally?
                Math.abs(toX - piece.posX) == 1 && piece.posY + direction == toY &&
                // if it is not the first move
                !board.moves.isEmpty() &&
                // is the pawn that you take is a different color?
                board.History.getLast().getPiece(piece.posY, toX).color != piece.color &&
                // is the last move was made by the pawn being en passant?
//                board.History.getLast().getPiece(piece.posY, toX) == board.moves.getLast().piece &&
                board.History.getLast().getPiece(piece.posY, toX) == board.History.get(board.History.size() - 2).getPiece(piece.posY + direction * 2, toX) &&
                // was it a 2 square move?
                Math.abs(board.moves.getLast().fromY - board.moves.getLast().toY) == 2) {

            if (PieceMoves.movePiece(board, piece, toY, toX)) {

                // make the enPassant pawn null to make it taken
                board.History.getLast().board[piece.posY][toX] = null;
                // you are able to en passant
                return true;
            }
        }
        return false;
    }
}
