package atrochess_moves;

import java.util.ArrayList;

public final class KnightMoves {

    protected static boolean moveKnight(Piece piece, int toY, int toX) {

        // if the knight moves +1 to one direction and +2 to the other
        return Math.min(Math.abs(toY - piece.posY), Math.abs(toX - piece.posX)) == 1 && Math.max(Math.abs(toY - piece.posY), Math.abs(toX - piece.posX)) == 2;
    }

    protected static boolean canKnightMove(Board board, Piece piece) {

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 2, piece.posX + 1)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 1, piece.posX + 2)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 2, piece.posX - 1)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 1, piece.posX - 2)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 2, piece.posX + 1)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 1, piece.posX + 2)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 2, piece.posX - 1)) {
            return true;
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 1, piece.posX - 2)) {
            return true;
        }
        return false;
    }

    protected static ArrayList<ChessMove> getAllKnightMoves(Board board, Piece piece) {

        ArrayList<ChessMove> chessMoves = new ArrayList<ChessMove>();

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 2, piece.posX + 1)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY + 2, piece.posX + 1));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 1, piece.posX + 2)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY + 1, piece.posX + 2));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 2, piece.posX - 1)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY + 2, piece.posX - 1));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY + 1, piece.posX - 2)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY + 1, piece.posX - 2));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 2, piece.posX + 1)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY - 2, piece.posX + 1));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 1, piece.posX + 2)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY - 1, piece.posX + 2));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 2, piece.posX - 1)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY - 2, piece.posX - 1));
        }

        if (PieceMoves.isMoveLegal(board, piece, piece.posY - 1, piece.posX - 2)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY - 1, piece.posX - 2));
        }
        return chessMoves;
    }
}
