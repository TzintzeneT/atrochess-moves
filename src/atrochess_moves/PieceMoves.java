package atrochess_moves;

import java.util.ArrayList;

public class PieceMoves {

    protected static boolean movePiece(Board board, Piece piece, int toY, int toX) {

        // check if game is active
//        if (board.gameState != GameState.Active) {
//            System.out.println("game is over, wth?");
//            return false;
//        }

//        if (board.isCheckmate()) {
//            System.out.println("you are checkmated, wth");
//            return false;
//        }
//
//        if (board.isStalemate()) {
//            System.out.println("you are stalemated, wth");
//            return false;
//        }

        if (PieceMoves.isMoveLegal(board, piece, toY, toX)) {
            // create the move
            ChessMove newChessMove = new ChessMove(piece.posY, piece.posX, toY, toX);
            // create and add new position
            Position newPosition = board.History.getLast().copyPosition();
            newPosition.board[toY][toX] = piece;
            newPosition.board[piece.posY][piece.posX] = null;
            board.History.add(newPosition);
            piece.move(toY, toX);
            board.moves.add(newChessMove);
            return true;
        }
//        System.out.println("hi2");
        return false;
    }

    public static boolean canPieceMove(Board board, Piece piece) {
        if (piece == null) {
            return false;
        }
        return switch (piece.name) {
            case "Pawn" -> PawnMoves.canPawnMove(board, piece);
            case "Bishop" -> BishopMoves.canBishopMove(board, piece);
            case "Rook" -> RookMoves.canRookMove(board, piece);
            case "Knight" -> KnightMoves.canKnightMove(board, piece);
            case "Queen" -> QueenMoves.canQueenMove(board, piece);
            case "King" -> KingMoves.canKingMove(board, piece);
            default -> false;
        };
    }

    public static ArrayList<ChessMove> getAllPieceMoves(Board board, Piece piece) {

        if (piece == null) {
            return new ArrayList<ChessMove>();
        }
        return switch (piece.name) {
            case "Pawn" -> PawnMoves.getAllPawnMoves(board, piece);
            case "Bishop" -> BishopMoves.getAllBishopMoves(board, piece);
            case "Rook" -> RookMoves.getAllRookMoves(board, piece);
            case "Knight" -> KnightMoves.getAllKnightMoves(board, piece);
            case "Queen" -> QueenMoves.getALlQueenMoves(board, piece);
            case "King" -> KingMoves.getAllKingMoves(board, piece);
            default -> new ArrayList<ChessMove>();
        };
    }

    public static boolean canMovePiece(Board board, Piece piece, int toY, int toX) {

        // cant go out of the board you bozo
        if (toX >= board.History.getFirst().board.length || toX < 0 || toY >= board.History.getFirst().board[0].length || toY < 0) {
            return false;
        }

//        System.out.println("rarrrr");

        // if the piece sent is not the piece at that location
        if (board.History.getLast().getPiece(piece.posY, piece.posX) != piece) {
//            System.out.println("you sent a different piece than the one that was there you bozo");
//            System.out.println(board.History.getLast().getPiece(piece.posY, piece.posX) + " != " + piece);
            return false;
        }

//        System.out.println("blah");

        // cant take your own pieces
        if (board.History.getLast().getPiece(toY, toX) != null && board.History.getLast().getPiece(toY, toX).color == piece.color) {
            return false;
        }

//        System.out.println("blash sakjnsajjn");

        // if the piece hasn't been moved
        if (piece.posY == toY && piece.posX == toX) {
//            System.out.println("You cannot move a piece to the same square it is already on");
            return false;
        }

//        System.out.println("jesus" + piece.name);

        return switch (piece.name) {
            case "Pawn" -> PawnMoves.movePawn(board, piece, toY, toX);
            case "Bishop" -> BishopMoves.moveBishop(board, piece, toY, toX);
            case "Rook" -> RookMoves.moveRook(board, piece, toY, toX);
            case "Knight" -> KnightMoves.moveKnight(piece, toY, toX);
            case "Queen" -> QueenMoves.moveQueen(board, piece, toY, toX);
            case "King" -> KingMoves.moveKing(board, (GKing) piece, toY, toX);
            default -> false;
        };
    }

    public static boolean isPieceAttacking(Board board, Piece piece, int toY, int toX) {

        // cant go out of the board you bozo
        if (toX >= board.History.getFirst().board.length || toX < 0 || toY >= board.History.getFirst().board[0].length || toY < 0) {
            return false;
        }

        // if the piece sent is not the piece at that location
        if (board.History.getLast().getPiece(piece.posY, piece.posX) != piece) {
//            System.out.println("you sent a different piece than the one that was there you bozo");
//            System.out.println(board.History.getLast().getPiece(piece.posY, piece.posX) + " != " + piece);
            return false;
        }

        // if the piece hasn't been moved
        if (piece.posY == toY && piece.posX == toX) {
//            System.out.println("You cannot move a piece to the same square it is already on");
            return false;
        }

        return switch (piece.name) {
            case "Pawn" -> PawnMoves.isPawnAttacking(board, piece, toY, toX);
            case "Bishop" -> BishopMoves.moveBishop(board, piece, toY, toX);
            case "Rook" -> RookMoves.moveRook(board, piece, toY, toX);
            case "Knight" -> KnightMoves.moveKnight(piece, toY, toX);
            case "Queen" -> QueenMoves.moveQueen(board, piece, toY, toX);
            case "King" -> KingMoves.isKingAttacking(board, piece, toY, toX);
            default -> false;
        };
    }

    public static boolean isMoveLegal(Board board, Piece piece, int toY, int toX) {

//        System.out.println("waaaaaaa");
        // cannot play with nothing
        if (piece == null) {
            return false;
        }
//        System.out.println("kqwjnk");

        // is it your turn
        if (piece.color != board.getTurnColor()) {
//            System.out.println("not your turn, bozo! its " + board.getTurnColor() + "'s turn");
//            System.out.println("moving " + piece.color);
            return false;
        }

//        System.out.println("weeeeeee");

        // can you move that piece there
        if (!PieceMoves.canMovePiece(board, piece, toY, toX)) {
            return false;
        }

//        System.out.println("lama");

        // create and add a new testing position
        Position testPosition = board.History.getLast().copyPosition();
        testPosition.board[toY][toX] = piece;
        testPosition.board[piece.posY][piece.posX] = null;
        // backup
        int oldY = piece.posY;
        int oldX = piece.posX;
        // move the piece
        piece.posY = toY;
        piece.posX = toX;

        board.History.add(testPosition);

        // are you in check in the new position?
        if (board.isInCheck(piece.color)) {
            // not a legal move then, go back
            board.History.removeLast();
            piece.posY = oldY;
            piece.posX = oldX;
            return false;
        }
        // is is legal move, remove traces
        board.History.removeLast();
        piece.posY = oldY;
        piece.posX = oldX;

        return true;
    }
}
