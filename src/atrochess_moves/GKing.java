package atrochess_moves;

public final class GKing extends Piece {

    protected boolean hasMoved = false;

    public GKing(int color){
        super("King", color, 0);
    }

    protected GKing clonePiece() {
        GKing temp = new GKing(this.color);
        temp.posY = this.posY;
        temp.posX = this.posX;
        temp.hasMoved = this.hasMoved;
        return temp;
    }

    @Override
    protected void move(int y, int x) {
        this.posX = x;
        this.posY = y;
        this.hasMoved = true;
    }
}
