package atrochess_moves;

import java.util.ArrayList;

public final class KingMoves {

    protected static boolean moveKing(Board board, GKing piece, int toY, int toX) {

        // cant into a check
        if (board.isSquareAttacked(board.getOppositeColor(piece.color), toY, toX)) {
            return false;
        }

        // if moving only 1 square
        if (Math.abs(piece.posY - toY) < 2 && Math.abs(piece.posX - toX) < 2) {
            return true;
        }

        // if moving 2 squares only in the X axis (castling) and king hasn't moved yet
        if (piece.posY - toY == 0 && Math.abs(piece.posX - toX) == 2 && !piece.hasMoved) {

            final Position currentPosition = board.History.getLast();
            // get the relevant yAxis
            final int yAxis = piece.color * (currentPosition.board[0].length - 1);

            // if castling left
            if (piece.posX - toX > 0) {

                // get the piece at the left rook starting square
                Piece allegedlyRook = currentPosition.getPiece(yAxis, 0);
                // if it is a rook and a real one, not a promoted pawn
                if (allegedlyRook != null && allegedlyRook.getClass().equals(Rook.class)) {
                    Rook actualRook = (Rook) allegedlyRook;
                    // and it didn't move
                    if (!actualRook.hasMoved) {
                        // check all of the squares in the way
                        for (int i = 1; i < piece.posX; i++) {
                            // if the way is blocked
                            if (currentPosition.getPiece(yAxis, i) != null) {
                                return false;
                            }
                        }
                        // if both king is not in check & rook is not gonna be threatened return true
                        return !board.isSquareAttacked(board.getOppositeColor(piece.color), piece.posY, piece.posX) && !board.isSquareAttacked(board.getOppositeColor(piece.color), piece.posY, piece.posX - 1);
                    }
                }
            } else {

                // get the piece at the right rook starting square
                Piece allegedlyRook = currentPosition.getPiece(yAxis, currentPosition.board.length - 1);
                // if it is a rook and a real one, not a promoted pawn
                if (allegedlyRook != null && allegedlyRook.getClass().equals(Rook.class)) {
                    Rook actualRook = (Rook) allegedlyRook;
                    // and it didn't move
                    if (!actualRook.hasMoved) {
                        // check all of the squares in the way
                        for (int i = currentPosition.board.length - 2; i > piece.posX; i--) {
                            // if the way is blocked
                            if (currentPosition.getPiece(yAxis, i) != null) {
                                return false;
                            }
                        }
                        // if both king is not in check & rook is not gonna be threatened return true
                        return !board.isSquareAttacked(board.getOppositeColor(piece.color), piece.posY, piece.posX) && !board.isSquareAttacked(board.getOppositeColor(piece.color), piece.posY, piece.posX + 1);
                    }
                }
            }
        }
        return false;
    }

    protected static boolean canKingMove(Board board, Piece piece) {
        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {

                // if it can, it is not a stalemate
                if (PieceMoves.isMoveLegal(board, piece, piece.posY + y, piece.posX + x)) {
                    return true;
                }
            }
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY, piece.posX + 2)) {
            return true;
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY, piece.posX - 2)) {
            return true;
        }

        return false;
    }

    protected static ArrayList<ChessMove> getAllKingMoves(Board board, Piece piece) {

        ArrayList<ChessMove> chessMoves = new ArrayList<ChessMove>();

        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {

                // if it can, it is not a stalemate
                if (PieceMoves.isMoveLegal(board, piece, piece.posY + y, piece.posX + x)) {
                    chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY + y, piece.posX + x));
                }
            }
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY, piece.posX + 2)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY, piece.posX + 2));
        }
        if (PieceMoves.isMoveLegal(board, piece, piece.posY, piece.posX - 2)) {
            chessMoves.add(new ChessMove(piece.posY, piece.posX, piece.posY, piece.posX - 2));
        }

        return chessMoves;
    }

    protected static boolean isKingAttacking(Board board, Piece piece, int toY, int toX) {

        // is the king near?
        return Math.abs(piece.posY - toY) < 2 && Math.abs(piece.posX - toX) < 2;
    }

    protected static boolean castle(Board board, Piece piece, int toY, int toX) {


        // if king is castling
        if (piece != null && piece.getClass().equals(GKing.class) && Math.abs(piece.posX - toX) > 1) {

            // if king can castle
            if (PieceMoves.movePiece(board, piece, toY, toX)) {

                // calculate the castling side
                int castleSide = piece.posX - toX;

                // calc witch y level it is, based on the color
                int yAxis = piece.color * (board.History.getLast().board.length -1);


                // if castling left
                if (castleSide > 0) {

                    // get rook at left for this color
                    Piece rook = board.History.getLast().getPiece(yAxis, 0);
                    // set the square it was in to null
                    board.History.getLast().board[yAxis][0] = null;

                    // move that rook to the new square
                    rook.move(yAxis, piece.posX - 1);
                    board.History.getLast().board[yAxis][piece.posX - 1] = rook;

                    // the move was done
                    return true;


                    // if castle right
                } else {

                    // get rook at right for this color
                    Piece rook = board.History.getLast().getPiece(yAxis, board.History.getLast().board[0].length -1);
                    // set the square it was in to null
                    board.History.getLast().board[yAxis][board.History.getLast().board[0].length -1] = null;

                    // move that rook to the new square
                    rook.move(yAxis, piece.posX + 1);
                    board.History.getLast().board[yAxis][piece.posX + 1] = rook;

                    // the move was done
                    return true;
                }

            } else {
                return false;
            }
        }
        return false;
    }
}
