package atrochess_moves;

import java.util.ArrayList;

public class Board {

    protected final ArrayList<Position> History = new ArrayList<Position>();
    protected final ArrayList<ChessMove> moves = new ArrayList<ChessMove>();
    protected GameState gameState;
    private final ArrayList<GKing> gkings;
    protected final Position defaultPosition;

    public Board() {

        this.gkings = new ArrayList<>();

        defaultPosition = new Position(
                new Piece[][]{
                        {new Rook(0), new Knight(0), new Bishop(0), this.getGKing(0), new Queen(0), new Bishop(0), new Knight(0), new Rook(0)},
                        {new Pawn(0), new Pawn(0), new Pawn(0), new Pawn(0), new Pawn(0), new Pawn(0), new Pawn(0), new Pawn(0)},
                        {null, null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null, null},
                        {null, null, null, null, null, null, null, null},
                        {new Pawn(1), new Pawn(1), new Pawn(1), new Pawn(1), new Pawn(1), new Pawn(1), new Pawn(1), new Pawn(1)},
                        {new Rook(1), new Knight(1), new Bishop(1), this.getGKing(1), new Queen(1), new Bishop(1), new Knight(1), new Rook(1)}
                }
        );

//        this.History = new ArrayList<Position>();
        normalizeToDefaultPosition();
        this.History.add(defaultPosition);
//        moves = new ArrayList<ChessMove>();
        this.gameState = GameState.Active;
    }

    protected Board(Position defaultPosition, ArrayList<GKing> gkings) {

        this.defaultPosition = defaultPosition;
        normalizeToDefaultPosition();

        this.History.add(this.defaultPosition);
        this.gkings = gkings;
        this.gameState = GameState.Active;
    }

    protected void normalizeToDefaultPosition() {
        for (int i = 0; i < defaultPosition.board.length; i++) {
            for (int j = 0; j < defaultPosition.board[i].length; j++) {
                if (defaultPosition.getPiece(i, j) != null) {

                    // set the position, but don't move the pieces
                    defaultPosition.getPiece(i, j).posY = i;
                    defaultPosition.getPiece(i, j).posX = j;
                }
            }
        }
    }

    protected GKing getGKing(int color) {

        if (gkings.size() <= color) {
            while (gkings.size() < color) {
                gkings.add(null);
            }
            gkings.add(color, new GKing(color));
        }
        if (gkings.get(color) == null) {
            gkings.set(color, new GKing(color));
        }

        return this.gkings.get(color);
    }

    public int getBoardYLength() {
        return this.History.getLast().board.length;
    }

    public int getBoardXLength() {
        return this.History.getLast().board[0].length;
    }

    public int getOppositeColor(int color) {
        return color * -1 + 1;
    }

    public int getTurnNumber() {
        return this.History.size() - 1;
    }

    public int getTurnColor() {
        return (this.History.size() - 1) % 2;
    }

    public String getGameState() {
        return this.gameState.name();
    }

    public boolean isGameActive() {
        return this.gameState == GameState.Active;
    }

    public boolean isInCheck(int color) {
        return isSquareAttacked(getOppositeColor(color), this.getGKing(color).posY, this.getGKing(color).posX);
    }

    protected boolean isCheckmate() {

        if (!isInCheck(getTurnColor())) {
            return false;
        }

        // for each piece on the board
        for (Piece[] pieces : this.History.getLast().board) {
            for (Piece piece : pieces) {

                // if it can move & if it is from the same color as the checked king
                if (piece != null && piece.color == getTurnColor() && PieceMoves.canPieceMove(this, piece)) {
                    return false;
                }
            }
        }
        return true;
    }

    private int howManyPiecesOnBoard(int moveNumber) {

        //the move number is a move that does not exist
        if (moveNumber >= this.History.size()) {
            return 0;
        }

        int counter = 0;
        for (Piece[] pieces : this.History.get(moveNumber).board) {
            for (Piece piece : pieces) {
                if (piece != null) {
                    counter++;
                }
            }
        }
        return counter;
    }

    private boolean insufficientMaterial(int color) {
        int material = 0;
        for (Piece[] pieces : this.History.getLast().board) {
            for (Piece piece : pieces) {
                if (piece != null && piece.color == color) {
                    switch (piece.name) {
                        case "Pawn", "Queen", "Rook":
                            return false;
                        case "Knight":
                            material++;
                        case "Bishop":
                            material += 2;
                    }
                    if (material > 2) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected boolean isStalemate() {

        // has the number of pieces didn't change in the last 50 modes?
        if (this.History.size() > 100 && howManyPiecesOnBoard(this.History.size() - 1) == howManyPiecesOnBoard(this.History.size() - 101)) {
            return true;
        }

        // is both players have insufficient material?
        if (insufficientMaterial(0) && insufficientMaterial(1)) {
            return true;
        }

        // has the position repeated 3 times already?
        if (threeFoldRepetition()) {
            return true;
        }

        // is the current player in check and none of the above is true
        if (isInCheck(getTurnColor())) {
            return false;
        }

        // for each piece on the board
        for (Piece[] pieces : this.History.getLast().board) {
            for (Piece piece : pieces) {

                // if it can move & if it is from the same color as the checked king
                if (piece != null && piece.color == getTurnColor() && PieceMoves.canPieceMove(this, piece)) {
                    return false;
                }
            }
        }
        return true;
    }

    protected void updateGameState() {

        if (this.isCheckmate()) {
            this.gameState = GameState.Checkmate;

        } else if (this.isStalemate()) {
            this.gameState = GameState.Stalemate;
        }
    }

    private boolean threeFoldRepetition() {

        // count how many times the position was repeated
        int counter = 0;

        // for each position
        for (int i = 0; i < getTurnNumber(); i++) {
            // is it equals to the last position
            if (this.History.get(i).equals(this.History.getLast())) {
                counter++;
            }
        }
        return counter > 2;
    }

    public boolean applyMove(String move) {

        if (this.gameState != GameState.Active) {
            throw new IllegalStateException("game is over");
//            return false;
        }

        // decode the chess move
        ChessMove chessMove = AlgebraicNotations.decode(this, move);

        // if it is not legal, return false
        if (chessMove == null) {
            throw new IllegalStateException("Illegal move, cannot operate.");
//            return false;
        }

        // if it is a promotion, promote
        if (move.indexOf('=') > -1) {

            if (promoteAPawn(chessMove.fromY, chessMove.fromX, chessMove.toY, chessMove.toX, chessMove.promoteTo)) {
                updateGameState();
                return true;
            }
            throw new IllegalStateException("Illegal promotion");
//            return false;
        }

        // otherwise, move the piece to the destination
        if (movePiece(chessMove.fromY, chessMove.fromX, chessMove.toY, chessMove.toX)) {
            updateGameState();
            return true;
        }
        throw new IllegalStateException("Illegal " + chessMove);
//        return false;
    }

    public boolean applyMove(ChessMove chessMove) {

        if (this.gameState != GameState.Active) {
            throw new IllegalStateException("game is over");
//            return false;
        }

        // if it is not legal, return false
        if (chessMove == null) {
            throw new IllegalStateException("chess move is null");
//            return false;
        }

        // if is promotion, promote
        if (chessMove.promoteTo != null) {

            if (promoteAPawn(chessMove.fromY, chessMove.fromX, chessMove.toY, chessMove.toX, chessMove.promoteTo)) {
                updateGameState();
                return true;
            }
            throw new IllegalStateException("Illegal promotion");
//            return false;
        }

        // otherwise, move the piece to the destination
        if (movePiece(chessMove.fromY, chessMove.fromX, chessMove.toY, chessMove.toX)) {
            updateGameState();
            return true;
        }
        throw new IllegalStateException("Illegal " + chessMove + "with piece " + this.History.getLast().getPiece(chessMove.fromY, chessMove.fromX));
//        return false;
    }

    protected boolean movePiece(int fromY, int fromX, int toY, int toX) {

        // if king is moving +2
        // king.castle

        // if pawn is moving sideways & !to a place with a piece
        // pawn.enPassant

        Piece pieceToMove = this.History.getLast().getPiece(fromY, fromX);
        System.out.println(this);
        System.out.println("moving: " + pieceToMove);

        if (pieceToMove == null) {
            throw new IllegalStateException("Illegal piece move: no piece");
//            return false;
        }

        // if king is castling
        if (pieceToMove.getClass().equals(GKing.class) && Math.abs(fromX - toX) > 1) {

            if (KingMoves.castle(this, pieceToMove, toY, toX)) {
                return true;
            }
        }

        if (pieceToMove.name.equals("Pawn") && pieceToMove.posX - toX != 0 && this.History.getLast().board[toY][toX] == null) {
            if (PawnMoves.enPassant(this, pieceToMove, toY, toX)) {
                return true;
            }
        }

//        System.out.println("hi");
        return PieceMoves.movePiece(this, pieceToMove, toY, toX);
    }

    protected boolean promoteAPawn(int fromY, int fromX, int toY, int toX, String promotionName) {
        return PawnMoves.promotePawn(this, this.History.getLast().getPiece(fromY, fromX), toY, toX, promotionName);
    }

    public ArrayList<ChessMove> getAllAvailableMoves() {

        ArrayList<ChessMove> availableMoves = new ArrayList<ChessMove>();

        for (Piece[] pieces : this.History.getLast().board) {
            for (Piece piece : pieces) {
                if (piece != null && piece.color == getTurnColor()) {
                    availableMoves.addAll(PieceMoves.getAllPieceMoves(this, piece));
                }
            }
        }
//        System.out.println(availableMoves);
        return availableMoves;
    }

    public boolean isSquareAttacked(int byColor, int y, int x) {
        for (Piece[] pieces : this.History.getLast().board) {
            for (Piece piece : pieces) {

                // check if the piece can go there
                if (piece != null && piece.color == byColor && PieceMoves.isPieceAttacking(this, piece, y, x)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Deprecated
    public Position[] getCopyOfHistory() {
        Position[] temp = new Position[this.History.size()];

        for (int i = 0; i < this.History.size(); i++) {
            temp[i] = this.History.get(i).clone();
        }
        return temp;
    }

    @Override
    public Board clone() {

        // create array for new kings
        ArrayList<GKing> gkings = new ArrayList<>();
        // clone the default position
        Position clonedDefPos = this.defaultPosition.clone();

        // add new kings to the new array
        for (Piece[] pieces : clonedDefPos.board) {
            for (Piece piece : pieces) {
                if (piece != null && piece.getName().equals("King")) {
                    gkings.add(piece.getColor(), (GKing) piece);
                }
            }
        }

        // create new board
        Board clonedBoard = new Board(clonedDefPos, gkings);

        // clone & apply all moves
        for (ChessMove chessMove : this.moves) {
            ChessMove clonedMove = chessMove.clone();
            clonedBoard.applyMove(clonedMove);
        }

        return clonedBoard;
    }

    @Override
    public String toString() {
        return "current position:\n" + this.History.getLast().toString() + "\ntotal moves: " + this.getTurnNumber() + "\nGame state: " + this.gameState.toString();
    }
}
