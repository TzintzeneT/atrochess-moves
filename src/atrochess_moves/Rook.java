package atrochess_moves;

public final class Rook extends Piece {
    protected boolean hasMoved = false;
    public Rook(int color){
        super("Rook", color, 5);
    }

    @Override
    protected void move(int y, int x){
        this.posX = x;
        this.posY = y;
        this.hasMoved = true;
    }

    protected Rook clonePiece() {
        Rook temp = new Rook(this.color);
        temp.posY = this.posY;
        temp.posX = this.posX;
        temp.hasMoved = this.hasMoved;
        return temp;
    }
}
