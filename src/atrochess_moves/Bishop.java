package atrochess_moves;

public final class Bishop extends Piece {
    public Bishop(int color){
        super("Bishop", color, 3);
    }

    protected Bishop clonePiece() {
        Bishop temp = new Bishop(this.color);
        temp.posY = this.posY;
        temp.posX = this.posX;
        return temp;
    }
}
