package atrochess_moves;

import java.util.HashMap;
import java.util.Map;

public class AlgebraicNotations {


    public static final Map<Character, Integer> algebraicConverter = new HashMap<Character, Integer>() {{
        put('h', 0);
        put('g', 1);
        put('f', 2);
        put('e', 3);
        put('d', 4);
        put('c', 5);
        put('b', 6);
        put('a', 7);
        put('1', 0);
        put('2', 1);
        put('3', 2);
        put('4', 3);
        put('5', 4);
        put('6', 5);
        put('7', 6);
        put('8', 7);
    }};

    public static final Map<Character, String> pieceTranslator = new HashMap<Character, String>() {{
        put('K', "King");
        put('Q', "Queen");
        put('R', "Rook");
        put('B', "Bishop");
        put('N', "Knight");
        put('P', "Pawn");
    }};

    public static int charToYPos(char location) {

        return location - 96;
    }

    public static int charToXPos(char location) {

        return location - 47;
    }


    public static ChessMove decode(Board board, String move) {

        if (move.isEmpty()) {
            return null;
        }

        // clean spaces
        move = move.replaceAll(" ", "");

        // if castling
        if (move.charAt(0) == '0') {

            return decodeCastling(board, move);
        }

        // if promoting
        if (move.indexOf('=') != -1) {

            return decodePromotion(board, move);

        }

        // otherwise
        return decodeNorMove(board, move);
    }

    private static ChessMove decodeNorMove(Board board, String move) {

        String newMove = decodeRecursion(move, "");

        char pieceType = newMove.charAt(0);

        int toY = algebraicConverter.get(newMove.charAt(newMove.length() - 1));
        int toX = algebraicConverter.get(newMove.charAt(newMove.length() - 2));

        int fromY = -1;
        int fromX = -1;

        if (newMove.length() > 4) {

            fromY = algebraicConverter.get(newMove.charAt(newMove.length() - 3));
            fromX = algebraicConverter.get(newMove.charAt(newMove.length() - 4));
            return new ChessMove(fromY, fromX, toY, toX);

        } else if (newMove.length() > 3) {

            if (newMove.charAt(1) > 48 && newMove.charAt(1) < 57) {

                fromY = algebraicConverter.get(newMove.charAt(1));

                searchX:
                for (int i = 0; i < board.History.getLast().board[fromY].length; i++) {

                    Piece piece = board.History.getLast().getPiece(fromY, i);

                    if (piece != null && comparePiece(piece.name, pieceType) && PieceMoves.isMoveLegal(board, piece, toY, toX)) {
                        fromX = piece.posX;
                        return new ChessMove(fromY, fromX, toY, toX);
//                        break searchX;
                    }
                }

            } else {

                fromX = algebraicConverter.get(newMove.charAt(1));

                searchY:
                for (int i = 0; i < board.History.getLast().board.length; i++) {

                    Piece piece = board.History.getLast().getPiece(i, fromX);

                    if (piece != null && comparePiece(piece.name, pieceType) && PieceMoves.isMoveLegal(board, piece, toY, toX)) {
                        fromY = piece.posY;
                        return new ChessMove(fromY, fromX, toY, toX);
//                        break searchY;
                    }
                }
            }
        } else if (newMove.length() > 2) {

            searchPiece:
            for (Piece[] pieces : board.History.getLast().board) {
                for (Piece piece : pieces) {

                    if (piece != null && comparePiece(piece.name, pieceType) && PieceMoves.isMoveLegal(board, piece, toY, toX)) {
                        fromY = piece.posY;
                        fromX = piece.posX;
                        return new ChessMove(fromY, fromX, toY, toX);
//                        break searchPiece;
                    }
                }
            }
        }

        return null;

    }

    private static boolean comparePiece(String name, char letter) {
        return pieceTranslator.get(letter).equals(name);
    }

    private static String decodeRecursion(String move, String moveData) {

        // if it is the last char in the move String
        if (move.length() < 2) {
            // it is the piece data, so add it and return everything.
            return move + moveData;
        }

        // piece moved in that move
        char pieceType = move.charAt(0);
        // if piece is not specified, it is a pawn then
        if (pieceType < 64 || pieceType > 91) {
            move = "P" + move;
            return decodeRecursion(move, moveData);
        }

        // if a square is specified, add that to the data
        if ((move.charAt(move.length() - 1) > 48 && move.charAt(move.length() - 1) < 57) || (move.charAt(move.length() - 1) > 96 && move.charAt(move.length() - 1) < 105)) {
            moveData = move.charAt(move.length() - 1) + moveData;
        }

        // go over the next char in move
        move = move.substring(0, move.length() - 1);
        return decodeRecursion(move, moveData);

    }

    @Deprecated
    private static void decodeMove(String move) {
        // piece moved in that move
        char pieceType = move.charAt(0);
        // if piece is not specified, it is a pawn then
        if (pieceType < 64 || pieceType > 91) {
            pieceType = 'P';
        }
        // form what square
        String fromSquare = "";
        // to witch square
        String toSquare;
        //index of the parser current location
        int parserIndex = move.length() - 1;


        // move parser to the last specified location
        for (; parserIndex > 0 && (move.charAt(parserIndex) < 48 || move.charAt(parserIndex) > 57); parserIndex--) ;

        // store the destination square
        toSquare = "" + move.charAt(parserIndex - 1) + move.charAt(parserIndex);
        // move the parser to next location
        parserIndex -= 2;

        // skip if there is information of taking a piece (not relevant)
        if (move.charAt(parserIndex) == 'x') {
            parserIndex--;
        }

        // get the data about the original square (could be between 0-2 characters)
        for (; parserIndex >= 0 && move.charAt(parserIndex) != pieceType; parserIndex--) {

            // add each data char to the square data
            fromSquare = move.charAt(parserIndex) + fromSquare;
        }


        System.out.println(pieceType + " moves to " + toSquare + " from " + fromSquare);
    }

    private static ChessMove decodePromotion(Board board, String move) {

        int yAxis = (board.getOppositeColor(board.getTurnColor())) * (board.History.getLast().board.length - 3) + 1;
//        System.out.println(yAxis);
        String promoteTo = null;

        for (int i = move.length() - 1; i > 1; i--) {
//            System.out.println(pieceTranslator.get(move.charAt(i)));
            if (pieceTranslator.get(move.charAt(i)) != null) {
                promoteTo = pieceTranslator.get(move.charAt(i));
                break;
            }
        }

//        for (int i = 0; i < board.History.getLast().board[yAxis].length; i++) ;

        for (Piece piece : board.History.getLast().board[yAxis]) {
//            System.out.println(piece.debugString());


//            boolean foundMove = PieceMoves.isMoveLegal(board, piece, charToYPos(move.charAt(0)), charToXPos(move.charAt(1)));

//            System.out.println(algebraicConverter.get(move.charAt(0)) + " " + algebraicConverter.get(move.charAt(1)));

            int toX = algebraicConverter.get(move.charAt(0));
            int toY = algebraicConverter.get(move.charAt(1));


//            System.out.println(piece);
//            System.out.println(foundMove2);
            if (piece != null && piece.name.equals("Pawn") && PieceMoves.isMoveLegal(board, piece, toY, toX)) {

//                System.out.println("yay");
//                System.out.println(board);

                return new ChessMove(piece.posY, piece.posX, toY, toX, promoteTo);
            }

        }

//        System.out.println("promotion!");
        return null;
    }

    private static ChessMove decodeCastling(Board board, String move) {

        int yAxis = board.getTurnColor() * (board.History.getLast().board.length - 1) + 1;
        char xAxis = 'e';

        String fromSquare = "e" + yAxis;
        String toSquare;

        int fromY = algebraicConverter.get(fromSquare.charAt(1));
        int fromX = algebraicConverter.get(fromSquare.charAt(0));
        int toY = -1;
        int toX = -1;

        // how many 0 are there?
        switch (move.split("0").length) {

            // castle short
            case 2:

                xAxis += 2;
                toSquare = xAxis + "" + yAxis;

                toY = algebraicConverter.get(toSquare.charAt(1));
                toX = algebraicConverter.get(toSquare.charAt(1));

                break;

            // castle long
            case 3:

                xAxis -= 2;
                toSquare = xAxis + "" + yAxis;

                toY = algebraicConverter.get(toSquare.charAt(1));
                toX = algebraicConverter.get(toSquare.charAt(1));

                break;
        }
        return new ChessMove(fromY, fromX, toY, toX);
    }

    public static void main(String[] args) {
//        decode(new Board(), "0-0-0");
//        System.out.println(decodeRecursion("d6", ""));
        System.out.println(decode(new Board(), "Nc3"));
    }
}
