package atrochess_moves;

public class Position {
    public final Piece[][] board;

    public Position(Piece[][] board) {
        this.board = board;
    }

    public Piece getPiece(int y, int x) {
        return this.board[y][x];
    }

//    public String getPieceType(int y, int x) {
//
//        if (this.board[y][x] == null) {
//            return "";
//        }
//
//        return this.board[y][x].name;
//    }

    public int getPieceColor(int y, int x) {

        if (this.board[y][x] == null) {
            return -1;
        }

        return this.board[y][x].color;
    }

    protected Position copyPosition() {
        Piece[][] newBoard = new Piece[board.length][board[0].length];
        Position newPosition = new Position(newBoard);
        for (int i = 0; i < newBoard.length; i++) {
            for (int j = 0; j < newBoard[0].length; j++) {
                newBoard[i][j] = this.board[i][j];
            }
        }
        return newPosition;
    }

    @Override
    public Position clone() {
        Piece[][] newBoard = new Piece[board.length][board[0].length];
        Position newPosition = new Position(newBoard);
        for (int i = 0; i < newBoard.length; i++) {
            for (int j = 0; j < newBoard[0].length; j++) {
                if (this.board[i][j] != null) {
                    newBoard[i][j] = this.board[i][j].clonePiece();
                }
            }
        }
        return newPosition;
    }

    @Override
    public String toString() {
        String position = "";
        for (Piece[] pieces : board) {
            position += "{";
            for (Piece piece : pieces) {
                position += "{" + piece + "}, ";
            }
            position += "}\n";
        }
        return position;
    }

    public boolean equals(Position compareTo) {

        // if the position is not the same size
        if (this.board.length != compareTo.board.length || this.board[0].length != compareTo.board[0].length) {
            return false;
        }


        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[0].length; j++) {

                // if one of the ChessBoard.moves.pieces is null but the other is not
                if ((this.board[i][j] == null || compareTo.board[i][j] == null) && this.board[i][j] != compareTo.board[i][j]) {
                    return false;
                    // if they are both not null, and they do not have the same name
                } else if ((this.board[i][j] != null && compareTo.board[i][j] != null) && !this.board[i][j].name.equals(compareTo.board[i][j].name)) {
                    return false;
                }
            }
        }
        return true;
    }
}
