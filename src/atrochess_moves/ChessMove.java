package atrochess_moves;

public class ChessMove {
    //    protected final Piece piece;
    protected final int fromX;
    protected final int fromY;
    protected final int toX;
    protected final int toY;
    protected final String promoteTo;

    public ChessMove(int fromY, int fromX, int toY, int toX) {
//        this.piece = piece;
        this.fromY = fromY;
        this.fromX = fromX;
        this.toX = toX;
        this.toY = toY;
        this.promoteTo = null;
    }

    public ChessMove(int fromY, int fromX, int toY, int toX, String promoteTo) {

        this.fromY = fromY;
        this.fromX = fromX;
        this.toX = toX;
        this.toY = toY;
        this.promoteTo = promoteTo;
    }

    public boolean equals(ChessMove compareTo) {
//        return compareTo != null && piece.name.equals(compareTo.piece.name) && this.fromY == compareTo.fromY && this.fromX == compareTo.fromX && this.toY == compareTo.toY && this.toX == compareTo.toX;
        return compareTo != null && this.fromY == compareTo.fromY && this.fromX == compareTo.fromX && this.toY == compareTo.toY && this.toX == compareTo.toX;
    }

    @Override
    public String toString() {
//        String str = "";
//        str += this.piece.name.charAt(0);
//        str += (char) (97 + 7 - fromX);
//        str += fromY;
//        str += "x";
//        str += (char) (97 + 7 - toX);
//        str += toY;
//        return str;

        String str = "move: ";
//        str += this.piece.name + " moved ";
        str += "{y=" + fromY + ", x=" + fromX + "} => {";
        str += "y=" + toY + ", x=" + toX + "}";
        if (promoteTo != null) {
            str += " (promoting to: " + promoteTo + ")";
        }

        return str;
    }

    @Override
    public ChessMove clone() {
        return new ChessMove(this.fromY, this.fromX, this.toY, this.toX, this.promoteTo);
    }
}
