package atrochess_moves;

public final class Knight extends Piece {
    public Knight(int color){
        super("Knight", color, 3);
    }

    protected Knight clonePiece() {
        Knight temp = new Knight(this.color);
        temp.posY = this.posY;
        temp.posX = this.posX;
        return temp;
    }
}
