#!/bin/bash

name=$1
if [ "$name" = "" ]; then
    name="build"
fi

cd src/atrochess_moves
javac -d ../../build/ *.java
cd ../../build/

jar cfm $name.jar MANIFEST.MF ./atrochess_moves/*.class

# thanks to Giga Gavriel for helping with this script
