# AtroChess-moves
board moves for the upcoming AtroChess (pronounced like atrocious) chess engine.
<br>
This project is written in Java & developed by me under the awesome GPLv3 licence.

## note:
this version kinda works but it's bad & I am working on rewriting a lot of stuff.
I will upload the files & the generated jar as soon as it will be ready.

**what to expect:**
- the new version will use the maven build system
- the new version will let you create your own pieces using a TOML config file without changing the source code
- the new version will work better

### update:
I decided to rewrite **everything**.
this project was nice, but it was not well written nor was it built with the idea of being extendable beyond the original codebase & capabilities.
I want to create the most hackable fun chess platform with my own engine & library, and this codebase wasn't able to cut-it.
feel free to look at this codebase but stay tuned for a much better one coming in the near future.

### update 2:
The new codebase is available [here](https://gitlab.com/munchess/lib).

## Special thanks
**Thanks to Giga Gavriel for helping with the build.sh script and for being a good friend**
